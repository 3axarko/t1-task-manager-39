package ru.t1.zkovalenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.AbstractModel;

import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll(@NotNull String sortField);

    void add(@NotNull M model);

    void update(@NotNull M model);

    void clear();

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    void remove(@NotNull M model);

}
