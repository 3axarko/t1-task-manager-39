package ru.t1.zkovalenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @Update("UPDATE tm_task " +
            "SET user_id = #{userId}, " +
            "   name = #{name}, " +
            "   description = #{description}, " +
            "   status = #{status}," +
            "   project_id = #{projectId}" +
            " WHERE id = #{id}")
    void update(@NotNull Task task);

    @Insert("INSERT INTO tm_task " +
            "(id, created, user_id, name, description, status, project_id) " +
            "VALUES (#{id}, #{created}, #{userId}, #{name}, #{description}, #{status}, #{projectId})")
    void add(@NotNull Task task);

    @Select("SELECT * FROM tm_task order by #{sortField}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAll(@NotNull String sortField);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} order by #{sortField}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllByUserId(@Param("userId") @NotNull String userId, @Param("sortField") @NotNull String sortField);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearByUserId(String userId);

    @Select("SELECT * FROM tm_task WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneById(@NotNull String id);

    @Select("SELECT * from (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tm_task) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneByIndex(@NotNull Integer index);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void remove(@NotNull Task task);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneByIdUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tm_task WHERE user_id = #{userId}) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneByIndexUserId(@Param("userId") @NotNull String userId,
                                        @Param("index") @NotNull Integer index);

}