package ru.t1.zkovalenko.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Index is incorrect");
    }

}
